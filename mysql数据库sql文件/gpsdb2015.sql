/*
Navicat MySQL Data Transfer

Source Server         : connected
Source Server Version : 50711
Source Host           : localhost:3306
Source Database       : gpsdb2015

Target Server Type    : MYSQL
Target Server Version : 50711
File Encoding         : 65001

Date: 2016-03-04 16:40:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `alarmrecord`
-- ----------------------------
DROP TABLE IF EXISTS `alarmrecord`;
CREATE TABLE `alarmrecord` (
  `alarmId` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `childType` varchar(255) DEFAULT NULL,
  `driver` varchar(255) DEFAULT NULL,
  `endTime` datetime DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL,
  `gas1` double NOT NULL,
  `gas2` double NOT NULL,
  `latitude` double NOT NULL,
  `latitude1` double NOT NULL,
  `location` varchar(255) DEFAULT NULL,
  `location1` varchar(255) DEFAULT NULL,
  `longitude` double NOT NULL,
  `longitude1` double NOT NULL,
  `mileage1` double NOT NULL,
  `mileage2` double NOT NULL,
  `plateNo` varchar(255) DEFAULT NULL,
  `processed` int(11) NOT NULL,
  `startTime` datetime DEFAULT NULL,
  `station` int(11) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `timeSpan` double NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `velocity` double NOT NULL,
  `videoFileName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`alarmId`),
  UNIQUE KEY `alarmId` (`alarmId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of alarmrecord
-- ----------------------------
INSERT INTO `alarmrecord` VALUES ('1', '2016-03-04 16:28:00', '', null, null, '0', 'GpsOnline', null, '2016-03-04 16:28:00', null, '0', '0', '0', '0', null, null, '0', '0', '0', '0', '测B00002', '0', '2016-03-04 16:28:00', '0', 'New', '0', 'platform_alarm', '0', null);
INSERT INTO `alarmrecord` VALUES ('2', '2016-03-04 16:28:00', '', null, null, '0', '4', null, '2016-03-04 16:28:00', null, '0', '0', '37.405968', '0', null, null, '109.498508', '0', '0', '0', '测B00002', '0', '2016-03-04 16:25:14', '0', 'New', '0', 'terminal_state', '61', null);
INSERT INTO `alarmrecord` VALUES ('3', '2016-03-04 16:28:00', '', null, null, '0', '1', null, '2016-03-04 16:28:00', null, '0', '0', '37.405968', '0', null, null, '109.498508', '0', '0', '0', '测B00002', '0', '2016-03-04 16:25:14', '0', 'New', '0', 'terminal_state', '61', null);
INSERT INTO `alarmrecord` VALUES ('4', '2016-03-04 16:28:00', '', null, null, '0', '0', null, '2016-03-04 16:28:00', null, '0', '0', '37.405968', '0', null, null, '109.498508', '0', '0', '0', '测B00002', '0', '2016-03-04 16:25:14', '0', 'New', '0', 'terminal_state', '61', null);

-- ----------------------------
-- Table structure for `basicdata`
-- ----------------------------
DROP TABLE IF EXISTS `basicdata`;
CREATE TABLE `basicdata` (
  `baseId` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `meta` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent` varchar(255) DEFAULT NULL,
  `sn` int(11) NOT NULL,
  PRIMARY KEY (`baseId`),
  UNIQUE KEY `baseId` (`baseId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of basicdata
-- ----------------------------

-- ----------------------------
-- Table structure for `department`
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `depId` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `assoMan` varchar(255) DEFAULT NULL,
  `assoTel` varchar(255) DEFAULT NULL,
  `businessScope` varchar(255) DEFAULT NULL,
  `memNo` varchar(255) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `parentId` int(11) NOT NULL,
  `region` varchar(255) DEFAULT NULL,
  `roadPermitNo` varchar(255) DEFAULT NULL,
  `roadPermitWord` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  PRIMARY KEY (`depId`),
  UNIQUE KEY `depId` (`depId`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `name_2` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of department
-- ----------------------------

-- ----------------------------
-- Table structure for `driverinfo`
-- ----------------------------
DROP TABLE IF EXISTS `driverinfo`;
CREATE TABLE `driverinfo` (
  `driverId` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `appointment` datetime DEFAULT NULL,
  `appraisalIntegral` float DEFAULT NULL,
  `bgTitle` varchar(255) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `certificationDate` datetime DEFAULT NULL,
  `companyNo` varchar(255) DEFAULT NULL,
  `corp` varchar(255) DEFAULT NULL,
  `driverCode` varchar(255) DEFAULT NULL,
  `driverLicence` varchar(255) DEFAULT NULL,
  `driverName` varchar(255) DEFAULT NULL,
  `driverRfid` varchar(255) DEFAULT NULL,
  `drivingType` varchar(255) DEFAULT NULL,
  `examineYear` datetime DEFAULT NULL,
  `harnessesAge` smallint(6) DEFAULT NULL,
  `identityCard` varchar(255) DEFAULT NULL,
  `invalidDate` datetime DEFAULT NULL,
  `licenseAgency` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `mobilePhone` varchar(255) DEFAULT NULL,
  `monitorOrg` varchar(255) DEFAULT NULL,
  `monitorPhone` varchar(255) DEFAULT NULL,
  `nativePlace` varchar(255) DEFAULT NULL,
  `operatorId` int(11) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `photoFormat` int(11) DEFAULT NULL,
  `register` datetime DEFAULT NULL,
  `serviceLevel` int(11) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  `vehicleId` int(11) NOT NULL,
  PRIMARY KEY (`driverId`),
  UNIQUE KEY `driverId` (`driverId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of driverinfo
-- ----------------------------

-- ----------------------------
-- Table structure for `enclosure`
-- ----------------------------
DROP TABLE IF EXISTS `enclosure`;
CREATE TABLE `enclosure` (
  `enclosureId` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `alarmType` varchar(255) DEFAULT NULL,
  `byTime` bit(1) NOT NULL,
  `centerLat` double NOT NULL,
  `centerLng` double NOT NULL,
  `delay` int(11) NOT NULL,
  `enclosureType` varchar(255) DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `gpsId` varchar(255) DEFAULT NULL,
  `keyPoint` int(11) NOT NULL,
  `limitSpeed` bit(1) NOT NULL,
  `lineWidth` double NOT NULL,
  `maxSpeed` double NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `offsetDelay` int(11) NOT NULL,
  `plateNo` varchar(255) DEFAULT NULL,
  `points` varchar(255) DEFAULT NULL,
  `radius` double NOT NULL,
  `sn` int(11) NOT NULL,
  `startDate` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`enclosureId`),
  UNIQUE KEY `enclosureId` (`enclosureId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enclosure
-- ----------------------------

-- ----------------------------
-- Table structure for `enclosurebinding`
-- ----------------------------
DROP TABLE IF EXISTS `enclosurebinding`;
CREATE TABLE `enclosurebinding` (
  `bindId` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `depId` int(11) NOT NULL,
  `enclosureId` int(11) NOT NULL,
  `vehicleId` int(11) NOT NULL,
  PRIMARY KEY (`bindId`),
  UNIQUE KEY `bindId` (`bindId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enclosurebinding
-- ----------------------------

-- ----------------------------
-- Table structure for `ewaybill`
-- ----------------------------
DROP TABLE IF EXISTS `ewaybill`;
CREATE TABLE `ewaybill` (
  `billId` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `eContent` varchar(255) DEFAULT NULL,
  `plateColor` int(11) NOT NULL,
  `plateNo` varchar(255) DEFAULT NULL,
  `vehicleId` int(11) NOT NULL,
  PRIMARY KEY (`billId`),
  UNIQUE KEY `billId` (`billId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ewaybill
-- ----------------------------

-- ----------------------------
-- Table structure for `fuelchangerecord`
-- ----------------------------
DROP TABLE IF EXISTS `fuelchangerecord`;
CREATE TABLE `fuelchangerecord` (
  `enclosureId` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `fuel` double NOT NULL,
  `happenTime` datetime DEFAULT NULL,
  `latitude` double NOT NULL,
  `location` varchar(255) DEFAULT NULL,
  `longitude` double NOT NULL,
  `manual` varchar(255) DEFAULT NULL,
  `mileage` double NOT NULL,
  `plateNo` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`enclosureId`),
  UNIQUE KEY `enclosureId` (`enclosureId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fuelchangerecord
-- ----------------------------

-- ----------------------------
-- Table structure for `fuelconsumption`
-- ----------------------------
DROP TABLE IF EXISTS `fuelconsumption`;
CREATE TABLE `fuelconsumption` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `gas` double NOT NULL,
  `gas1` double NOT NULL,
  `gas2` double NOT NULL,
  `hour` double NOT NULL,
  `intervalDescr` varchar(255) DEFAULT NULL,
  `intervalType` int(11) NOT NULL,
  `mileage` double NOT NULL,
  `mileage1` double NOT NULL,
  `mileage2` double NOT NULL,
  `plateNo` varchar(255) DEFAULT NULL,
  `staticDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fuelconsumption
-- ----------------------------

-- ----------------------------
-- Table structure for `functionmodel`
-- ----------------------------
DROP TABLE IF EXISTS `functionmodel`;
CREATE TABLE `functionmodel` (
  `funcId` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `descr` varchar(255) DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `funcType` int(11) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `mask` int(11) DEFAULT NULL,
  `menuSort` int(11) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `url` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`funcId`),
  UNIQUE KEY `funcId` (`funcId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of functionmodel
-- ----------------------------

-- ----------------------------
-- Table structure for `govplatformmsg`
-- ----------------------------
DROP TABLE IF EXISTS `govplatformmsg`;
CREATE TABLE `govplatformmsg` (
  `funcId` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `msg` varchar(255) DEFAULT NULL,
  `msgType` varchar(255) DEFAULT NULL,
  `plateColor` varchar(255) DEFAULT NULL,
  `plateNo` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`funcId`),
  UNIQUE KEY `funcId` (`funcId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of govplatformmsg
-- ----------------------------

-- ----------------------------
-- Table structure for `gpsinfo`
-- ----------------------------
DROP TABLE IF EXISTS `gpsinfo`;
CREATE TABLE `gpsinfo` (
  `gpsId` int(11) NOT NULL AUTO_INCREMENT,
  `alarmState` int(11) NOT NULL,
  `altitude` double NOT NULL,
  `createDate` datetime DEFAULT NULL,
  `direction` int(11) NOT NULL,
  `gas` double NOT NULL,
  `latitude` double NOT NULL,
  `location` varchar(255) DEFAULT NULL,
  `longitude` double NOT NULL,
  `mileage` double NOT NULL,
  `plateNo` varchar(255) DEFAULT NULL,
  `recordVelocity` double NOT NULL,
  `runStatus` varchar(255) DEFAULT NULL,
  `sendTime` datetime DEFAULT NULL,
  `simNo` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `valid` bit(1) NOT NULL,
  `velocity` double NOT NULL,
  PRIMARY KEY (`gpsId`),
  UNIQUE KEY `gpsId` (`gpsId`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gpsinfo
-- ----------------------------
INSERT INTO `gpsinfo` VALUES ('1', '0', '500', '2016-03-04 16:28:00', '72', '19.900000000000002', '37.405968', null, '109.498508', '5720', '测B00002', '70.7', null, '2016-03-04 16:25:14', '13100000002', '19', '', '61');
INSERT INTO `gpsinfo` VALUES ('2', '0', '500', '2016-03-04 16:28:05', '74', '19.900000000000002', '37.405988', null, '109.498528', '5740', '测B00002', '70.7', null, '2016-03-04 16:28:04', '13100000002', '19', '', '61');
INSERT INTO `gpsinfo` VALUES ('3', '0', '500', '2016-03-04 16:28:09', '75', '19.900000000000002', '37.405998', null, '109.498538', '5750', '测B00002', '70.7', null, '2016-03-04 16:28:09', '13100000002', '19', '', '61');
INSERT INTO `gpsinfo` VALUES ('4', '0', '500', '2016-03-04 16:28:15', '76', '19.900000000000002', '37.406008', null, '109.498548', '5760', '测B00002', '70.7', null, '2016-03-04 16:28:14', '13100000002', '19', '', '61');
INSERT INTO `gpsinfo` VALUES ('5', '0', '500', '2016-03-04 16:28:20', '77', '19.900000000000002', '37.406017999999996', null, '109.49855799999999', '5770', '测B00002', '70.7', null, '2016-03-04 16:28:19', '13100000002', '19', '', '61');
INSERT INTO `gpsinfo` VALUES ('6', '0', '500', '2016-03-04 16:28:25', '78', '19.900000000000002', '37.406028', null, '109.49856799999999', '5780', '测B00002', '70.7', null, '2016-03-04 16:28:24', '13100000002', '19', '', '61');
INSERT INTO `gpsinfo` VALUES ('7', '0', '500', '2016-03-04 16:28:30', '79', '19.900000000000002', '37.406037999999995', null, '109.498578', '5790', '测B00002', '70.7', null, '2016-03-04 16:28:29', '13100000002', '19', '', '61');
INSERT INTO `gpsinfo` VALUES ('8', '8', '500', '2016-03-04 16:28:35', '80', '19.900000000000002', '37.406048', null, '109.498588', '5800', '测B00002', '70.8', null, '2016-03-04 16:28:34', '13100000002', '19', '', '62');
INSERT INTO `gpsinfo` VALUES ('9', '0', '500', '2016-03-04 16:31:19', '81', '19.900000000000002', '37.406058', null, '109.498598', '5810', '测B00002', '70.8', null, '2016-03-04 16:28:39', '13100000002', '19', '', '62');
INSERT INTO `gpsinfo` VALUES ('10', '0', '500', '2016-03-04 16:31:20', '82', '19.900000000000002', '37.406068', null, '109.49860799999999', '5820', '测B00002', '70.8', null, '2016-03-04 16:31:19', '13100000002', '19', '', '62');
INSERT INTO `gpsinfo` VALUES ('11', '0', '500', '2016-03-04 16:31:25', '83', '19.900000000000002', '37.406078', null, '109.498618', '5830', '测B00002', '70.8', null, '2016-03-04 16:31:24', '13100000002', '19', '', '62');
INSERT INTO `gpsinfo` VALUES ('12', '0', '500', '2016-03-04 16:31:30', '84', '19.900000000000002', '37.406088', null, '109.498628', '5840', '测B00002', '70.8', null, '2016-03-04 16:31:29', '13100000002', '19', '', '62');
INSERT INTO `gpsinfo` VALUES ('13', '0', '500', '2016-03-04 16:31:35', '85', '19.900000000000002', '37.406098', null, '109.498638', '5850', '测B00002', '70.8', null, '2016-03-04 16:31:34', '13100000002', '19', '', '62');
INSERT INTO `gpsinfo` VALUES ('14', '0', '500', '2016-03-04 16:31:40', '86', '19.900000000000002', '37.406107999999996', null, '109.49864799999999', '5860', '测B00002', '70.8', null, '2016-03-04 16:31:39', '13100000002', '19', '', '62');
INSERT INTO `gpsinfo` VALUES ('15', '0', '500', '2016-03-04 16:31:45', '87', '19.900000000000002', '37.406118', null, '109.49865799999999', '5870', '测B00002', '70.8', null, '2016-03-04 16:31:44', '13100000002', '19', '', '62');
INSERT INTO `gpsinfo` VALUES ('16', '0', '500', '2016-03-04 16:31:50', '88', '19.900000000000002', '37.406127999999995', null, '109.498668', '5880', '测B00002', '70.8', null, '2016-03-04 16:31:49', '13100000002', '19', '', '62');
INSERT INTO `gpsinfo` VALUES ('17', '0', '500', '2016-03-04 16:31:55', '89', '19.900000000000002', '37.406138', null, '109.498678', '5890', '测B00002', '70.8', null, '2016-03-04 16:31:54', '13100000002', '19', '', '62');
INSERT INTO `gpsinfo` VALUES ('18', '0', '500', '2016-03-04 16:32:00', '90', '19.900000000000002', '37.406148', null, '109.498688', '5900', '测B00002', '70.9', null, '2016-03-04 16:31:59', '13100000002', '19', '', '63');
INSERT INTO `gpsinfo` VALUES ('19', '0', '500', '2016-03-04 16:32:05', '91', '19.900000000000002', '37.406158', null, '109.49869799999999', '5910', '测B00002', '70.9', null, '2016-03-04 16:32:04', '13100000002', '19', '', '63');
INSERT INTO `gpsinfo` VALUES ('20', '0', '500', '2016-03-04 16:32:10', '92', '19.900000000000002', '37.406168', null, '109.498708', '5920', '测B00002', '70.9', null, '2016-03-04 16:32:09', '13100000002', '19', '', '63');
INSERT INTO `gpsinfo` VALUES ('21', '0', '500', '2016-03-04 16:32:15', '93', '19.900000000000002', '37.406178', null, '109.498718', '5930', '测B00002', '70.9', null, '2016-03-04 16:32:14', '13100000002', '19', '', '63');
INSERT INTO `gpsinfo` VALUES ('22', '0', '500', '2016-03-04 16:32:20', '94', '19.900000000000002', '37.406188', null, '109.498728', '5940', '测B00002', '70.9', null, '2016-03-04 16:32:19', '13100000002', '19', '', '63');
INSERT INTO `gpsinfo` VALUES ('23', '0', '500', '2016-03-04 16:32:25', '95', '19.900000000000002', '37.406197999999996', null, '109.49873799999999', '5950', '测B00002', '70.9', null, '2016-03-04 16:32:24', '13100000002', '19', '', '63');
INSERT INTO `gpsinfo` VALUES ('24', '0', '500', '2016-03-04 16:33:10', '96', '19.900000000000002', '37.406208', null, '109.49874799999999', '5960', '测B00002', '70.9', null, '2016-03-04 16:32:29', '13100000002', '19', '', '63');
INSERT INTO `gpsinfo` VALUES ('25', '0', '500', '2016-03-04 16:33:15', '98', '19.900000000000002', '37.406228', null, '109.498768', '5980', '测B00002', '70.9', null, '2016-03-04 16:33:14', '13100000002', '19', '', '63');
INSERT INTO `gpsinfo` VALUES ('26', '0', '500', '2016-03-04 16:33:20', '99', '19.900000000000002', '37.406237999999995', null, '109.498778', '5990', '测B00002', '70.9', null, '2016-03-04 16:33:19', '13100000002', '19', '', '63');
INSERT INTO `gpsinfo` VALUES ('27', '536612855', '500', '2016-03-04 16:33:25', '100', '19.900000000000002', '37.406248', null, '109.49878799999999', '6000', '测B00002', '71', null, '2016-03-04 16:33:24', '13100000002', '7168', '', '64');
INSERT INTO `gpsinfo` VALUES ('28', '0', '500', '2016-03-04 16:33:30', '101', '19.900000000000002', '37.406258', null, '109.498798', '6010', '测B00002', '71', null, '2016-03-04 16:33:29', '13100000002', '19', '', '64');
INSERT INTO `gpsinfo` VALUES ('29', '0', '500', '2016-03-04 16:33:35', '102', '19.900000000000002', '37.406268', null, '109.498808', '6020', '测B00002', '71', null, '2016-03-04 16:33:34', '13100000002', '19', '', '64');
INSERT INTO `gpsinfo` VALUES ('30', '0', '500', '2016-03-04 16:33:40', '103', '19.900000000000002', '37.406278', null, '109.498818', '6030', '测B00002', '71', null, '2016-03-04 16:33:39', '13100000002', '19', '', '64');
INSERT INTO `gpsinfo` VALUES ('31', '0', '500', '2016-03-04 16:33:45', '104', '19.900000000000002', '37.406287999999996', null, '109.49882799999999', '6040', '测B00002', '71', null, '2016-03-04 16:33:44', '13100000002', '19', '', '64');

-- ----------------------------
-- Table structure for `gpsmileage`
-- ----------------------------
DROP TABLE IF EXISTS `gpsmileage`;
CREATE TABLE `gpsmileage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `gasLastComp` double NOT NULL,
  `gasLastDay` double NOT NULL,
  `gasLastHour` double NOT NULL,
  `gasLastMonth` double NOT NULL,
  `lastCompTime` datetime DEFAULT NULL,
  `mileageLastComp` double NOT NULL,
  `mileageLastDay` double NOT NULL,
  `mileageLastHour` double NOT NULL,
  `mileageLastMonth` double NOT NULL,
  `plateNo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gpsmileage
-- ----------------------------

-- ----------------------------
-- Table structure for `gpsrealdata`
-- ----------------------------
DROP TABLE IF EXISTS `gpsrealdata`;
CREATE TABLE `gpsrealdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alarmState` varchar(255) DEFAULT NULL,
  `altitude` double NOT NULL,
  `depId` int(11) NOT NULL,
  `direction` int(11) NOT NULL,
  `dvrStatus` varchar(255) DEFAULT NULL,
  `enclosureType` int(11) NOT NULL,
  `gas` double NOT NULL,
  `latitude` double NOT NULL,
  `location` varchar(255) DEFAULT NULL,
  `longitude` double NOT NULL,
  `mileage` double NOT NULL,
  `online` bit(1) NOT NULL,
  `plateNo` varchar(255) DEFAULT NULL,
  `recordVelocity` double NOT NULL,
  `sendTime` datetime DEFAULT NULL,
  `simNo` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `valid` bit(1) NOT NULL,
  `vehicleId` int(11) NOT NULL,
  `velocity` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gpsrealdata
-- ----------------------------
INSERT INTO `gpsrealdata` VALUES ('1', '00000000000000000000000000000000', '500', '117440514', '104', null, '0', '19.900000000000002', '37.406287999999996', null, '109.49882799999999', '6040', '', '测B00002', '71', '2016-03-04 16:33:44', '13100000002', '00000000000000000000000000010011', '1970-01-01 08:00:00', '', '3', '64');

-- ----------------------------
-- Table structure for `jt809command`
-- ----------------------------
DROP TABLE IF EXISTS `jt809command`;
CREATE TABLE `jt809command` (
  `cmdId` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `SN` int(11) NOT NULL,
  `cmd` int(11) NOT NULL,
  `cmdData` varchar(255) DEFAULT NULL,
  `descr` varchar(255) DEFAULT NULL,
  `plateColor` tinyint(4) NOT NULL,
  `plateNo` varchar(255) DEFAULT NULL,
  `simNo` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `subCmd` int(11) NOT NULL,
  `updateDate` datetime DEFAULT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`cmdId`),
  UNIQUE KEY `cmdId` (`cmdId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of jt809command
-- ----------------------------

-- ----------------------------
-- Table structure for `linebufferpoint`
-- ----------------------------
DROP TABLE IF EXISTS `linebufferpoint`;
CREATE TABLE `linebufferpoint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `enclosureId` int(11) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `nodeNo` int(11) NOT NULL,
  `sortNo` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of linebufferpoint
-- ----------------------------

-- ----------------------------
-- Table structure for `linesegment`
-- ----------------------------
DROP TABLE IF EXISTS `linesegment`;
CREATE TABLE `linesegment` (
  `segId` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `alarmType` varchar(255) DEFAULT NULL,
  `byTime` bit(1) NOT NULL,
  `enclosureId` int(11) NOT NULL,
  `latitude1` double NOT NULL,
  `latitude2` double NOT NULL,
  `limitSpeed` bit(1) NOT NULL,
  `lineWidth` int(11) NOT NULL,
  `longitude1` double NOT NULL,
  `longitude2` double NOT NULL,
  `maxSpeed` int(11) NOT NULL,
  `maxTimeLimit` int(11) NOT NULL,
  `minTimeLimit` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `overSpeedTime` int(11) NOT NULL,
  `pointId` int(11) NOT NULL,
  `station` bit(1) NOT NULL,
  PRIMARY KEY (`segId`),
  UNIQUE KEY `segId` (`segId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of linesegment
-- ----------------------------

-- ----------------------------
-- Table structure for `mediaitem`
-- ----------------------------
DROP TABLE IF EXISTS `mediaitem`;
CREATE TABLE `mediaitem` (
  `mediaItemId` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `channelId` tinyint(4) NOT NULL,
  `codeFormat` tinyint(4) NOT NULL,
  `commandId` int(11) NOT NULL,
  `commandType` varchar(255) DEFAULT NULL,
  `eventCode` tinyint(4) NOT NULL,
  `fileName` varchar(255) DEFAULT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `mediaDataId` int(11) NOT NULL,
  `mediaType` tinyint(4) NOT NULL,
  `plateNo` varchar(255) DEFAULT NULL,
  `sendTime` datetime DEFAULT NULL,
  `simNo` varchar(255) DEFAULT NULL,
  `speed` double NOT NULL,
  PRIMARY KEY (`mediaItemId`),
  UNIQUE KEY `mediaItemId` (`mediaItemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mediaitem
-- ----------------------------

-- ----------------------------
-- Table structure for `memberinfo`
-- ----------------------------
DROP TABLE IF EXISTS `memberinfo`;
CREATE TABLE `memberinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `businessScope` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `contactPhone` varchar(255) DEFAULT NULL,
  `licenseNo` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `orgNo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of memberinfo
-- ----------------------------

-- ----------------------------
-- Table structure for `onlinestatic`
-- ----------------------------
DROP TABLE IF EXISTS `onlinestatic`;
CREATE TABLE `onlinestatic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `depId` int(11) NOT NULL,
  `depName` varchar(255) DEFAULT NULL,
  `intervalType` int(11) NOT NULL,
  `onlineNum` int(11) NOT NULL,
  `onlineRate` double NOT NULL,
  `parentDepId` int(11) NOT NULL,
  `statisticDate` datetime DEFAULT NULL,
  `vehicleNum` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of onlinestatic
-- ----------------------------

-- ----------------------------
-- Table structure for `operationlog`
-- ----------------------------
DROP TABLE IF EXISTS `operationlog`;
CREATE TABLE `operationlog` (
  `depId` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `userId` int(11) NOT NULL,
  `userName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`depId`),
  UNIQUE KEY `depId` (`depId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of operationlog
-- ----------------------------

-- ----------------------------
-- Table structure for `platformstate`
-- ----------------------------
DROP TABLE IF EXISTS `platformstate`;
CREATE TABLE `platformstate` (
  `stateId` int(11) NOT NULL AUTO_INCREMENT,
  `gpsServerDate` datetime DEFAULT NULL,
  `gpsServerState` varchar(255) DEFAULT NULL,
  `mainLinkDate` datetime DEFAULT NULL,
  `mainLinkState` varchar(255) DEFAULT NULL,
  `subLinkDate` datetime DEFAULT NULL,
  `subLinkState` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`stateId`),
  UNIQUE KEY `stateId` (`stateId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of platformstate
-- ----------------------------

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `roleId` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`roleId`),
  UNIQUE KEY `roleId` (`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------

-- ----------------------------
-- Table structure for `rolefunc`
-- ----------------------------
DROP TABLE IF EXISTS `rolefunc`;
CREATE TABLE `rolefunc` (
  `role_id` int(11) NOT NULL,
  `sys_func_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`sys_func_id`),
  KEY `FKF3F0507A418B4B7C` (`role_id`),
  KEY `FKF3F0507AA1C65F87` (`sys_func_id`),
  CONSTRAINT `FKF3F0507A418B4B7C` FOREIGN KEY (`role_id`) REFERENCES `role` (`roleId`),
  CONSTRAINT `FKF3F0507AA1C65F87` FOREIGN KEY (`sys_func_id`) REFERENCES `functionmodel` (`funcId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rolefunc
-- ----------------------------

-- ----------------------------
-- Table structure for `systemconfig`
-- ----------------------------
DROP TABLE IF EXISTS `systemconfig`;
CREATE TABLE `systemconfig` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `accessPoint_name` varchar(255) DEFAULT NULL,
  `alaram_wave_flag` int(11) DEFAULT NULL,
  `alarm_redo_end_time` int(11) DEFAULT NULL,
  `alarm_redo_time` int(11) DEFAULT NULL,
  `alarm_supervisor` varchar(255) DEFAULT NULL,
  `alarm_supervisor_mail` varchar(255) DEFAULT NULL,
  `alarm_supervisor_tel` varchar(255) DEFAULT NULL,
  `alarm_types` varchar(255) DEFAULT NULL,
  `auth_password` varchar(255) DEFAULT NULL,
  `auth_username` varchar(255) DEFAULT NULL,
  `authentication_code` varchar(255) DEFAULT NULL,
  `auto_post_query_cmd_inteval` int(11) DEFAULT NULL,
  `bath_delay_time` int(11) DEFAULT NULL,
  `car_control_password` varchar(255) DEFAULT NULL,
  `car_data_down_time` int(11) DEFAULT NULL,
  `center_server_status` int(11) DEFAULT NULL,
  `cross_region_flag` int(11) DEFAULT NULL,
  `driver_licence_valid` int(11) DEFAULT NULL,
  `driver_term_type` int(11) DEFAULT NULL,
  `driverschool_flag` int(11) DEFAULT NULL,
  `g3_media_ip` varchar(255) DEFAULT NULL,
  `g3_media_port` int(11) DEFAULT NULL,
  `google_key1` varchar(255) DEFAULT NULL,
  `google_key2` varchar(255) DEFAULT NULL,
  `google_url` varchar(255) DEFAULT NULL,
  `google_version` varchar(255) DEFAULT NULL,
  `gov_connect_info` int(11) DEFAULT NULL,
  `gov_sub_connect_ip` varchar(255) DEFAULT NULL,
  `gov_sub_connect_port` int(11) DEFAULT NULL,
  `gov_sub_connect_status` int(11) DEFAULT NULL,
  `goverment_picture_ftp_ip` varchar(255) DEFAULT NULL,
  `goverment_picture_ftp_port` int(11) DEFAULT NULL,
  `goverment_port` int(11) DEFAULT NULL,
  `government_account` varchar(255) DEFAULT NULL,
  `government_ip` varchar(255) DEFAULT NULL,
  `government_password` varchar(255) DEFAULT NULL,
  `govment_encrypt_flag` bit(1) DEFAULT NULL,
  `govment_ia1` bigint(20) DEFAULT NULL,
  `govment_ic1` bigint(20) DEFAULT NULL,
  `govment_m1` bigint(20) DEFAULT NULL,
  `govment_center_id` bigint(20) DEFAULT NULL,
  `gps_status_types` varchar(255) DEFAULT NULL,
  `gpsdata_delay_time` int(11) DEFAULT NULL,
  `group_show_all` int(11) DEFAULT NULL,
  `initLat` double DEFAULT NULL,
  `initLng` double DEFAULT NULL,
  `initZoomLevel` int(11) DEFAULT NULL,
  `js_debug_flag` int(11) DEFAULT NULL,
  `log_term_flag` int(11) DEFAULT NULL,
  `log_term_id` varchar(255) DEFAULT NULL,
  `logo_flag` int(11) DEFAULT NULL,
  `mapType` int(11) DEFAULT NULL,
  `mapabc_key1` varchar(255) DEFAULT NULL,
  `mapabc_key2` varchar(255) DEFAULT NULL,
  `package_size` int(11) DEFAULT NULL,
  `polling_online_compute_time` int(11) DEFAULT NULL,
  `pop_config` int(11) DEFAULT NULL,
  `post_query_interval` int(11) DEFAULT NULL,
  `show_car_code` int(11) DEFAULT NULL,
  `smart_key` varchar(255) DEFAULT NULL,
  `start_govment_connect` int(11) DEFAULT NULL,
  `static_date` datetime DEFAULT NULL,
  `superior_gov_status` int(11) DEFAULT NULL,
  `system_flag` int(11) DEFAULT NULL,
  `system_mode` int(11) DEFAULT NULL,
  `system_title` varchar(255) DEFAULT NULL,
  `taxi_flag` int(11) DEFAULT NULL,
  `terminal_types` varchar(255) DEFAULT NULL,
  `update_latlon_interval` int(11) DEFAULT NULL,
  `urgency_server_ip` varchar(255) DEFAULT NULL,
  `urgency_tcp_port` int(11) DEFAULT NULL,
  `urgency_udp_port` int(11) DEFAULT NULL,
  `yz_interface_ip` varchar(255) DEFAULT NULL,
  `yz_interface_port` bigint(20) DEFAULT NULL,
  `yz_password` varchar(255) DEFAULT NULL,
  `yz_user_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of systemconfig
-- ----------------------------

-- ----------------------------
-- Table structure for `terminal`
-- ----------------------------
DROP TABLE IF EXISTS `terminal`;
CREATE TABLE `terminal` (
  `termId` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `bind` bit(1) DEFAULT NULL,
  `devNo` varchar(20) DEFAULT NULL,
  `installAddress` varchar(80) DEFAULT NULL,
  `installTime` datetime DEFAULT NULL,
  `makeFactory` varchar(30) DEFAULT NULL,
  `makeNo` varchar(10) DEFAULT NULL,
  `makeTime` datetime DEFAULT NULL,
  `seqNo` varchar(255) DEFAULT NULL,
  `simNo` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `termNo` varchar(20) DEFAULT NULL,
  `termType` varchar(255) DEFAULT NULL,
  `updateTime` datetime DEFAULT NULL,
  `verHardware` smallint(6) DEFAULT NULL,
  `verProtocol` smallint(6) DEFAULT NULL,
  `verSoftware` smallint(6) DEFAULT NULL,
  `waitor` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`termId`),
  UNIQUE KEY `termId` (`termId`),
  UNIQUE KEY `termNo` (`termNo`),
  UNIQUE KEY `termNo_2` (`termNo`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of terminal
-- ----------------------------
INSERT INTO `terminal` VALUES ('1', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:46', '123', '123', '2016-03-04 16:27:46', null, null, null, '1000000', '123', '2016-03-04 16:27:46', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('2', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:46', '123', '123', '2016-03-04 16:27:46', null, null, null, '1000001', '123', '2016-03-04 16:27:46', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('3', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:46', '123', '123', '2016-03-04 16:27:46', null, null, null, '1000002', '123', '2016-03-04 16:27:46', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('4', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:46', '123', '123', '2016-03-04 16:27:46', null, null, null, '1000003', '123', '2016-03-04 16:27:46', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('5', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:46', '123', '123', '2016-03-04 16:27:46', null, null, null, '1000004', '123', '2016-03-04 16:27:46', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('6', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:46', '123', '123', '2016-03-04 16:27:46', null, null, null, '1000005', '123', '2016-03-04 16:27:46', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('7', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:46', '123', '123', '2016-03-04 16:27:46', null, null, null, '1000006', '123', '2016-03-04 16:27:46', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('8', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:46', '123', '123', '2016-03-04 16:27:46', null, null, null, '1000007', '123', '2016-03-04 16:27:46', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('9', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:46', '123', '123', '2016-03-04 16:27:46', null, null, null, '1000008', '123', '2016-03-04 16:27:46', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('10', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:46', '123', '123', '2016-03-04 16:27:46', null, null, null, '1000009', '123', '2016-03-04 16:27:46', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('11', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:46', '123', '123', '2016-03-04 16:27:46', null, null, null, '1000010', '123', '2016-03-04 16:27:46', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('12', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:46', '123', '123', '2016-03-04 16:27:46', null, null, null, '1000011', '123', '2016-03-04 16:27:46', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('13', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:46', '123', '123', '2016-03-04 16:27:46', null, null, null, '1000012', '123', '2016-03-04 16:27:46', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('14', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:47', '123', '123', '2016-03-04 16:27:47', null, null, null, '1000013', '123', '2016-03-04 16:27:47', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('15', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:47', '123', '123', '2016-03-04 16:27:47', null, null, null, '1000014', '123', '2016-03-04 16:27:47', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('16', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:47', '123', '123', '2016-03-04 16:27:47', null, null, null, '1000015', '123', '2016-03-04 16:27:47', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('17', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:47', '123', '123', '2016-03-04 16:27:47', null, null, null, '1000016', '123', '2016-03-04 16:27:47', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('18', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:47', '123', '123', '2016-03-04 16:27:47', null, null, null, '1000017', '123', '2016-03-04 16:27:47', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('19', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:47', '123', '123', '2016-03-04 16:27:47', null, null, null, '1000018', '123', '2016-03-04 16:27:47', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('20', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:47', '123', '123', '2016-03-04 16:27:47', null, null, null, '1000019', '123', '2016-03-04 16:27:47', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('21', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:47', '123', '123', '2016-03-04 16:27:47', null, null, null, '1000020', '123', '2016-03-04 16:27:47', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('22', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:47', '123', '123', '2016-03-04 16:27:47', null, null, null, '1000021', '123', '2016-03-04 16:27:47', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('23', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:47', '123', '123', '2016-03-04 16:27:47', null, null, null, '1000022', '123', '2016-03-04 16:27:47', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('24', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:47', '123', '123', '2016-03-04 16:27:47', null, null, null, '1000023', '123', '2016-03-04 16:27:47', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('25', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:47', '123', '123', '2016-03-04 16:27:47', null, null, null, '1000024', '123', '2016-03-04 16:27:47', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('26', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:47', '123', '123', '2016-03-04 16:27:47', null, null, null, '1000025', '123', '2016-03-04 16:27:47', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('27', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:47', '123', '123', '2016-03-04 16:27:47', null, null, null, '1000026', '123', '2016-03-04 16:27:47', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('28', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:47', '123', '123', '2016-03-04 16:27:47', null, null, null, '1000027', '123', '2016-03-04 16:27:47', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('29', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:48', '123', '123', '2016-03-04 16:27:48', null, null, null, '1000028', '123', '2016-03-04 16:27:48', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('30', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:48', '123', '123', '2016-03-04 16:27:48', null, null, null, '1000029', '123', '2016-03-04 16:27:48', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('31', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:48', '123', '123', '2016-03-04 16:27:48', null, null, null, '1000030', '123', '2016-03-04 16:27:48', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('32', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:48', '123', '123', '2016-03-04 16:27:48', null, null, null, '1000031', '123', '2016-03-04 16:27:48', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('33', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:48', '123', '123', '2016-03-04 16:27:48', null, null, null, '1000032', '123', '2016-03-04 16:27:48', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('34', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:48', '123', '123', '2016-03-04 16:27:48', null, null, null, '1000033', '123', '2016-03-04 16:27:48', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('35', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:48', '123', '123', '2016-03-04 16:27:48', null, null, null, '1000034', '123', '2016-03-04 16:27:48', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('36', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:48', '123', '123', '2016-03-04 16:27:48', null, null, null, '1000035', '123', '2016-03-04 16:27:48', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('37', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:48', '123', '123', '2016-03-04 16:27:48', null, null, null, '1000036', '123', '2016-03-04 16:27:48', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('38', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:48', '123', '123', '2016-03-04 16:27:48', null, null, null, '1000037', '123', '2016-03-04 16:27:48', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('39', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:48', '123', '123', '2016-03-04 16:27:48', null, null, null, '1000038', '123', '2016-03-04 16:27:48', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('40', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:48', '123', '123', '2016-03-04 16:27:48', null, null, null, '1000039', '123', '2016-03-04 16:27:48', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('41', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:48', '123', '123', '2016-03-04 16:27:48', null, null, null, '1000040', '123', '2016-03-04 16:27:48', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('42', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:49', '123', '123', '2016-03-04 16:27:49', null, null, null, '1000041', '123', '2016-03-04 16:27:49', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('43', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:49', '123', '123', '2016-03-04 16:27:49', null, null, null, '1000042', '123', '2016-03-04 16:27:49', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('44', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:49', '123', '123', '2016-03-04 16:27:49', null, null, null, '1000043', '123', '2016-03-04 16:27:49', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('45', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:49', '123', '123', '2016-03-04 16:27:49', null, null, null, '1000044', '123', '2016-03-04 16:27:49', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('46', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:49', '123', '123', '2016-03-04 16:27:49', null, null, null, '1000045', '123', '2016-03-04 16:27:49', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('47', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:49', '123', '123', '2016-03-04 16:27:49', null, null, null, '1000046', '123', '2016-03-04 16:27:49', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('48', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:49', '123', '123', '2016-03-04 16:27:49', null, null, null, '1000047', '123', '2016-03-04 16:27:49', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('49', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:49', '123', '123', '2016-03-04 16:27:49', null, null, null, '1000048', '123', '2016-03-04 16:27:49', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('50', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:49', '123', '123', '2016-03-04 16:27:49', null, null, null, '1000049', '123', '2016-03-04 16:27:49', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('51', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:49', '123', '123', '2016-03-04 16:27:49', null, null, null, '1000050', '123', '2016-03-04 16:27:49', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('52', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:49', '123', '123', '2016-03-04 16:27:49', null, null, null, '1000051', '123', '2016-03-04 16:27:49', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('53', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:49', '123', '123', '2016-03-04 16:27:49', null, null, null, '1000052', '123', '2016-03-04 16:27:49', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('54', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:49', '123', '123', '2016-03-04 16:27:49', null, null, null, '1000053', '123', '2016-03-04 16:27:49', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('55', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:50', '123', '123', '2016-03-04 16:27:50', null, null, null, '1000054', '123', '2016-03-04 16:27:50', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('56', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:50', '123', '123', '2016-03-04 16:27:50', null, null, null, '1000055', '123', '2016-03-04 16:27:50', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('57', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:50', '123', '123', '2016-03-04 16:27:50', null, null, null, '1000056', '123', '2016-03-04 16:27:50', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('58', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:50', '123', '123', '2016-03-04 16:27:50', null, null, null, '1000057', '123', '2016-03-04 16:27:50', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('59', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:50', '123', '123', '2016-03-04 16:27:50', null, null, null, '1000058', '123', '2016-03-04 16:27:50', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('60', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:50', '123', '123', '2016-03-04 16:27:50', null, null, null, '1000059', '123', '2016-03-04 16:27:50', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('61', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:50', '123', '123', '2016-03-04 16:27:50', null, null, null, '1000060', '123', '2016-03-04 16:27:50', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('62', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:50', '123', '123', '2016-03-04 16:27:50', null, null, null, '1000061', '123', '2016-03-04 16:27:50', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('63', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:50', '123', '123', '2016-03-04 16:27:50', null, null, null, '1000062', '123', '2016-03-04 16:27:50', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('64', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:50', '123', '123', '2016-03-04 16:27:50', null, null, null, '1000063', '123', '2016-03-04 16:27:50', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('65', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:50', '123', '123', '2016-03-04 16:27:50', null, null, null, '1000064', '123', '2016-03-04 16:27:50', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('66', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:50', '123', '123', '2016-03-04 16:27:50', null, null, null, '1000065', '123', '2016-03-04 16:27:50', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('67', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:50', '123', '123', '2016-03-04 16:27:50', null, null, null, '1000066', '123', '2016-03-04 16:27:50', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('68', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:50', '123', '123', '2016-03-04 16:27:50', null, null, null, '1000067', '123', '2016-03-04 16:27:50', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('69', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:50', '123', '123', '2016-03-04 16:27:50', null, null, null, '1000068', '123', '2016-03-04 16:27:50', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('70', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:50', '123', '123', '2016-03-04 16:27:50', null, null, null, '1000069', '123', '2016-03-04 16:27:50', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('71', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:51', '123', '123', '2016-03-04 16:27:51', null, null, null, '1000070', '123', '2016-03-04 16:27:51', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('72', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:51', '123', '123', '2016-03-04 16:27:51', null, null, null, '1000071', '123', '2016-03-04 16:27:51', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('73', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:51', '123', '123', '2016-03-04 16:27:51', null, null, null, '1000072', '123', '2016-03-04 16:27:51', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('74', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:51', '123', '123', '2016-03-04 16:27:51', null, null, null, '1000073', '123', '2016-03-04 16:27:51', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('75', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:51', '123', '123', '2016-03-04 16:27:51', null, null, null, '1000074', '123', '2016-03-04 16:27:51', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('76', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:51', '123', '123', '2016-03-04 16:27:51', null, null, null, '1000075', '123', '2016-03-04 16:27:51', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('77', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:51', '123', '123', '2016-03-04 16:27:51', null, null, null, '1000076', '123', '2016-03-04 16:27:51', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('78', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:51', '123', '123', '2016-03-04 16:27:51', null, null, null, '1000077', '123', '2016-03-04 16:27:51', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('79', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:51', '123', '123', '2016-03-04 16:27:51', null, null, null, '1000078', '123', '2016-03-04 16:27:51', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('80', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:51', '123', '123', '2016-03-04 16:27:51', null, null, null, '1000079', '123', '2016-03-04 16:27:51', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('81', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:51', '123', '123', '2016-03-04 16:27:51', null, null, null, '1000080', '123', '2016-03-04 16:27:51', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('82', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:51', '123', '123', '2016-03-04 16:27:51', null, null, null, '1000081', '123', '2016-03-04 16:27:51', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('83', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:51', '123', '123', '2016-03-04 16:27:51', null, null, null, '1000082', '123', '2016-03-04 16:27:51', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('84', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:51', '123', '123', '2016-03-04 16:27:51', null, null, null, '1000083', '123', '2016-03-04 16:27:51', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('85', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:52', '123', '123', '2016-03-04 16:27:52', null, null, null, '1000084', '123', '2016-03-04 16:27:52', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('86', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:52', '123', '123', '2016-03-04 16:27:52', null, null, null, '1000085', '123', '2016-03-04 16:27:52', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('87', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:52', '123', '123', '2016-03-04 16:27:52', null, null, null, '1000086', '123', '2016-03-04 16:27:52', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('88', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:52', '123', '123', '2016-03-04 16:27:52', null, null, null, '1000087', '123', '2016-03-04 16:27:52', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('89', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:52', '123', '123', '2016-03-04 16:27:52', null, null, null, '1000088', '123', '2016-03-04 16:27:52', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('90', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:52', '123', '123', '2016-03-04 16:27:52', null, null, null, '1000089', '123', '2016-03-04 16:27:52', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('91', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:52', '123', '123', '2016-03-04 16:27:52', null, null, null, '1000090', '123', '2016-03-04 16:27:52', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('92', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:52', '123', '123', '2016-03-04 16:27:52', null, null, null, '1000091', '123', '2016-03-04 16:27:52', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('93', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:52', '123', '123', '2016-03-04 16:27:52', null, null, null, '1000092', '123', '2016-03-04 16:27:52', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('94', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:52', '123', '123', '2016-03-04 16:27:52', null, null, null, '1000093', '123', '2016-03-04 16:27:52', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('95', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:52', '123', '123', '2016-03-04 16:27:52', null, null, null, '1000094', '123', '2016-03-04 16:27:52', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('96', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:52', '123', '123', '2016-03-04 16:27:52', null, null, null, '1000095', '123', '2016-03-04 16:27:52', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('97', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:52', '123', '123', '2016-03-04 16:27:52', null, null, null, '1000096', '123', '2016-03-04 16:27:52', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('98', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:52', '123', '123', '2016-03-04 16:27:52', null, null, null, '1000097', '123', '2016-03-04 16:27:52', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('99', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:52', '123', '123', '2016-03-04 16:27:52', null, null, null, '1000098', '123', '2016-03-04 16:27:52', '0', '0', '0', '');
INSERT INTO `terminal` VALUES ('100', '1970-01-01 08:00:00', '', null, null, '0', '', '123', '', '2016-03-04 16:27:52', '123', '123', '2016-03-04 16:27:52', null, null, null, '1000099', '123', '2016-03-04 16:27:52', '0', '0', '0', '');

-- ----------------------------
-- Table structure for `terminalcommand`
-- ----------------------------
DROP TABLE IF EXISTS `terminalcommand`;
CREATE TABLE `terminalcommand` (
  `cmdId` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `SN` int(11) NOT NULL,
  `cmd` varchar(255) DEFAULT NULL,
  `cmdData` varchar(255) DEFAULT NULL,
  `cmdType` int(11) DEFAULT NULL,
  `plateNo` varchar(255) DEFAULT NULL,
  `simNo` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `vehicleId` int(11) NOT NULL,
  PRIMARY KEY (`cmdId`),
  UNIQUE KEY `cmdId` (`cmdId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of terminalcommand
-- ----------------------------

-- ----------------------------
-- Table structure for `terminalparam`
-- ----------------------------
DROP TABLE IF EXISTS `terminalparam`;
CREATE TABLE `terminalparam` (
  `paramId` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `commandId` int(11) NOT NULL,
  `fieldType` varchar(255) DEFAULT NULL,
  `plateNo` varchar(255) DEFAULT NULL,
  `sN` int(11) NOT NULL,
  `simNo` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`paramId`),
  UNIQUE KEY `paramId` (`paramId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of terminalparam
-- ----------------------------

-- ----------------------------
-- Table structure for `userdepartment`
-- ----------------------------
DROP TABLE IF EXISTS `userdepartment`;
CREATE TABLE `userdepartment` (
  `userId` int(11) NOT NULL,
  `depId` int(11) NOT NULL,
  PRIMARY KEY (`userId`,`depId`),
  KEY `FK23CDD59D2BB198BE` (`depId`),
  KEY `FK23CDD59D5D8DBDC1` (`userId`),
  CONSTRAINT `FK23CDD59D2BB198BE` FOREIGN KEY (`depId`) REFERENCES `department` (`depId`),
  CONSTRAINT `FK23CDD59D5D8DBDC1` FOREIGN KEY (`userId`) REFERENCES `userinfo` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of userdepartment
-- ----------------------------

-- ----------------------------
-- Table structure for `userinfo`
-- ----------------------------
DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE `userinfo` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `loginName` varchar(50) NOT NULL,
  `mapCenterLat` double NOT NULL,
  `mapCenterLng` double NOT NULL,
  `mapLevel` int(11) NOT NULL,
  `mapType` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(16) NOT NULL,
  `userFlag` int(11) NOT NULL,
  `userState` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `userId` (`userId`),
  UNIQUE KEY `loginName` (`loginName`),
  UNIQUE KEY `loginName_2` (`loginName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of userinfo
-- ----------------------------

-- ----------------------------
-- Table structure for `userrole`
-- ----------------------------
DROP TABLE IF EXISTS `userrole`;
CREATE TABLE `userrole` (
  `userId` int(11) NOT NULL,
  `roleId` int(11) NOT NULL,
  PRIMARY KEY (`userId`,`roleId`),
  KEY `FKF3F76701B850FE09` (`roleId`),
  KEY `FKF3F767015D8DBDC1` (`userId`),
  CONSTRAINT `FKF3F767015D8DBDC1` FOREIGN KEY (`userId`) REFERENCES `userinfo` (`userId`),
  CONSTRAINT `FKF3F76701B850FE09` FOREIGN KEY (`roleId`) REFERENCES `role` (`roleId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of userrole
-- ----------------------------

-- ----------------------------
-- Table structure for `vehicle`
-- ----------------------------
DROP TABLE IF EXISTS `vehicle`;
CREATE TABLE `vehicle` (
  `vehicleId` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `buyTime` datetime DEFAULT NULL,
  `depId` int(11) NOT NULL,
  `depName` varchar(255) DEFAULT NULL,
  `driver` varchar(255) DEFAULT NULL,
  `driverMobile` varchar(255) DEFAULT NULL,
  `gpsTerminalType` varchar(255) DEFAULT NULL,
  `industry` varchar(255) DEFAULT NULL,
  `installDate` datetime DEFAULT NULL,
  `lastCheckTime` datetime DEFAULT NULL,
  `memberId` int(11) NOT NULL,
  `monitor` varchar(255) DEFAULT NULL,
  `monitorMobile` varchar(255) DEFAULT NULL,
  `motorID` varchar(255) DEFAULT NULL,
  `offlineTime` datetime DEFAULT NULL,
  `onlineTime` datetime DEFAULT NULL,
  `operPermit` varchar(255) DEFAULT NULL,
  `plateColor` int(11) NOT NULL,
  `plateNo` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `routes` varchar(255) DEFAULT NULL,
  `runStatus` varchar(255) DEFAULT NULL,
  `simNo` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `termId` int(11) NOT NULL,
  `useType` varchar(255) DEFAULT NULL,
  `vehicleType` varchar(255) DEFAULT NULL,
  `vendor` varchar(255) DEFAULT NULL,
  `videoDeviceId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`vehicleId`),
  UNIQUE KEY `vehicleId` (`vehicleId`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vehicle
-- ----------------------------
INSERT INTO `vehicle` VALUES ('1', '2016-03-04 16:27:46', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:46', null, '0', null, null, null, null, null, null, '2', '测B00000', null, null, 'normal', '13100000000', null, '1', null, null, null, null);
INSERT INTO `vehicle` VALUES ('2', '2016-03-04 16:27:46', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:46', null, '0', null, null, null, null, null, null, '2', '测B00001', null, null, 'normal', '13100000001', null, '2', null, null, null, null);
INSERT INTO `vehicle` VALUES ('3', '2016-03-04 16:27:46', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:46', null, '0', null, null, null, null, null, null, '2', '测B00002', null, null, 'normal', '13100000002', null, '3', null, null, null, null);
INSERT INTO `vehicle` VALUES ('4', '2016-03-04 16:27:46', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:46', null, '0', null, null, null, null, null, null, '2', '测B00003', null, null, 'normal', '13100000003', null, '4', null, null, null, null);
INSERT INTO `vehicle` VALUES ('5', '2016-03-04 16:27:46', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:46', null, '0', null, null, null, null, null, null, '2', '测B00004', null, null, 'normal', '13100000004', null, '5', null, null, null, null);
INSERT INTO `vehicle` VALUES ('6', '2016-03-04 16:27:46', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:46', null, '0', null, null, null, null, null, null, '2', '测B00005', null, null, 'normal', '13100000005', null, '6', null, null, null, null);
INSERT INTO `vehicle` VALUES ('7', '2016-03-04 16:27:46', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:46', null, '0', null, null, null, null, null, null, '2', '测B00006', null, null, 'normal', '13100000006', null, '7', null, null, null, null);
INSERT INTO `vehicle` VALUES ('8', '2016-03-04 16:27:46', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:46', null, '0', null, null, null, null, null, null, '2', '测B00007', null, null, 'normal', '13100000007', null, '8', null, null, null, null);
INSERT INTO `vehicle` VALUES ('9', '2016-03-04 16:27:46', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:46', null, '0', null, null, null, null, null, null, '2', '测B00008', null, null, 'normal', '13100000008', null, '9', null, null, null, null);
INSERT INTO `vehicle` VALUES ('10', '2016-03-04 16:27:46', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:46', null, '0', null, null, null, null, null, null, '2', '测B00009', null, null, 'normal', '13100000009', null, '10', null, null, null, null);
INSERT INTO `vehicle` VALUES ('11', '2016-03-04 16:27:46', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:46', null, '0', null, null, null, null, null, null, '2', '测B00010', null, null, 'normal', '13100000010', null, '11', null, null, null, null);
INSERT INTO `vehicle` VALUES ('12', '2016-03-04 16:27:46', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:46', null, '0', null, null, null, null, null, null, '2', '测B00011', null, null, 'normal', '13100000011', null, '12', null, null, null, null);
INSERT INTO `vehicle` VALUES ('13', '2016-03-04 16:27:47', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:47', null, '0', null, null, null, null, null, null, '2', '测B00012', null, null, 'normal', '13100000012', null, '13', null, null, null, null);
INSERT INTO `vehicle` VALUES ('14', '2016-03-04 16:27:47', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:47', null, '0', null, null, null, null, null, null, '2', '测B00013', null, null, 'normal', '13100000013', null, '14', null, null, null, null);
INSERT INTO `vehicle` VALUES ('15', '2016-03-04 16:27:47', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:47', null, '0', null, null, null, null, null, null, '2', '测B00014', null, null, 'normal', '13100000014', null, '15', null, null, null, null);
INSERT INTO `vehicle` VALUES ('16', '2016-03-04 16:27:47', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:47', null, '0', null, null, null, null, null, null, '2', '测B00015', null, null, 'normal', '13100000015', null, '16', null, null, null, null);
INSERT INTO `vehicle` VALUES ('17', '2016-03-04 16:27:47', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:47', null, '0', null, null, null, null, null, null, '2', '测B00016', null, null, 'normal', '13100000016', null, '17', null, null, null, null);
INSERT INTO `vehicle` VALUES ('18', '2016-03-04 16:27:47', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:47', null, '0', null, null, null, null, null, null, '2', '测B00017', null, null, 'normal', '13100000017', null, '18', null, null, null, null);
INSERT INTO `vehicle` VALUES ('19', '2016-03-04 16:27:47', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:47', null, '0', null, null, null, null, null, null, '2', '测B00018', null, null, 'normal', '13100000018', null, '19', null, null, null, null);
INSERT INTO `vehicle` VALUES ('20', '2016-03-04 16:27:47', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:47', null, '0', null, null, null, null, null, null, '2', '测B00019', null, null, 'normal', '13100000019', null, '20', null, null, null, null);
INSERT INTO `vehicle` VALUES ('21', '2016-03-04 16:27:47', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:47', null, '0', null, null, null, null, null, null, '2', '测B00020', null, null, 'normal', '13100000020', null, '21', null, null, null, null);
INSERT INTO `vehicle` VALUES ('22', '2016-03-04 16:27:47', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:47', null, '0', null, null, null, null, null, null, '2', '测B00021', null, null, 'normal', '13100000021', null, '22', null, null, null, null);
INSERT INTO `vehicle` VALUES ('23', '2016-03-04 16:27:47', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:47', null, '0', null, null, null, null, null, null, '2', '测B00022', null, null, 'normal', '13100000022', null, '23', null, null, null, null);
INSERT INTO `vehicle` VALUES ('24', '2016-03-04 16:27:47', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:47', null, '0', null, null, null, null, null, null, '2', '测B00023', null, null, 'normal', '13100000023', null, '24', null, null, null, null);
INSERT INTO `vehicle` VALUES ('25', '2016-03-04 16:27:47', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:47', null, '0', null, null, null, null, null, null, '2', '测B00024', null, null, 'normal', '13100000024', null, '25', null, null, null, null);
INSERT INTO `vehicle` VALUES ('26', '2016-03-04 16:27:47', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:47', null, '0', null, null, null, null, null, null, '2', '测B00025', null, null, 'normal', '13100000025', null, '26', null, null, null, null);
INSERT INTO `vehicle` VALUES ('27', '2016-03-04 16:27:47', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:47', null, '0', null, null, null, null, null, null, '2', '测B00026', null, null, 'normal', '13100000026', null, '27', null, null, null, null);
INSERT INTO `vehicle` VALUES ('28', '2016-03-04 16:27:48', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:48', null, '0', null, null, null, null, null, null, '2', '测B00027', null, null, 'normal', '13100000027', null, '28', null, null, null, null);
INSERT INTO `vehicle` VALUES ('29', '2016-03-04 16:27:48', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:48', null, '0', null, null, null, null, null, null, '2', '测B00028', null, null, 'normal', '13100000028', null, '29', null, null, null, null);
INSERT INTO `vehicle` VALUES ('30', '2016-03-04 16:27:48', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:48', null, '0', null, null, null, null, null, null, '2', '测B00029', null, null, 'normal', '13100000029', null, '30', null, null, null, null);
INSERT INTO `vehicle` VALUES ('31', '2016-03-04 16:27:48', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:48', null, '0', null, null, null, null, null, null, '2', '测B00030', null, null, 'normal', '13100000030', null, '31', null, null, null, null);
INSERT INTO `vehicle` VALUES ('32', '2016-03-04 16:27:48', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:48', null, '0', null, null, null, null, null, null, '2', '测B00031', null, null, 'normal', '13100000031', null, '32', null, null, null, null);
INSERT INTO `vehicle` VALUES ('33', '2016-03-04 16:27:48', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:48', null, '0', null, null, null, null, null, null, '2', '测B00032', null, null, 'normal', '13100000032', null, '33', null, null, null, null);
INSERT INTO `vehicle` VALUES ('34', '2016-03-04 16:27:48', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:48', null, '0', null, null, null, null, null, null, '2', '测B00033', null, null, 'normal', '13100000033', null, '34', null, null, null, null);
INSERT INTO `vehicle` VALUES ('35', '2016-03-04 16:27:48', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:48', null, '0', null, null, null, null, null, null, '2', '测B00034', null, null, 'normal', '13100000034', null, '35', null, null, null, null);
INSERT INTO `vehicle` VALUES ('36', '2016-03-04 16:27:48', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:48', null, '0', null, null, null, null, null, null, '2', '测B00035', null, null, 'normal', '13100000035', null, '36', null, null, null, null);
INSERT INTO `vehicle` VALUES ('37', '2016-03-04 16:27:48', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:48', null, '0', null, null, null, null, null, null, '2', '测B00036', null, null, 'normal', '13100000036', null, '37', null, null, null, null);
INSERT INTO `vehicle` VALUES ('38', '2016-03-04 16:27:48', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:48', null, '0', null, null, null, null, null, null, '2', '测B00037', null, null, 'normal', '13100000037', null, '38', null, null, null, null);
INSERT INTO `vehicle` VALUES ('39', '2016-03-04 16:27:48', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:48', null, '0', null, null, null, null, null, null, '2', '测B00038', null, null, 'normal', '13100000038', null, '39', null, null, null, null);
INSERT INTO `vehicle` VALUES ('40', '2016-03-04 16:27:48', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:48', null, '0', null, null, null, null, null, null, '2', '测B00039', null, null, 'normal', '13100000039', null, '40', null, null, null, null);
INSERT INTO `vehicle` VALUES ('41', '2016-03-04 16:27:49', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:49', null, '0', null, null, null, null, null, null, '2', '测B00040', null, null, 'normal', '13100000040', null, '41', null, null, null, null);
INSERT INTO `vehicle` VALUES ('42', '2016-03-04 16:27:49', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:49', null, '0', null, null, null, null, null, null, '2', '测B00041', null, null, 'normal', '13100000041', null, '42', null, null, null, null);
INSERT INTO `vehicle` VALUES ('43', '2016-03-04 16:27:49', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:49', null, '0', null, null, null, null, null, null, '2', '测B00042', null, null, 'normal', '13100000042', null, '43', null, null, null, null);
INSERT INTO `vehicle` VALUES ('44', '2016-03-04 16:27:49', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:49', null, '0', null, null, null, null, null, null, '2', '测B00043', null, null, 'normal', '13100000043', null, '44', null, null, null, null);
INSERT INTO `vehicle` VALUES ('45', '2016-03-04 16:27:49', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:49', null, '0', null, null, null, null, null, null, '2', '测B00044', null, null, 'normal', '13100000044', null, '45', null, null, null, null);
INSERT INTO `vehicle` VALUES ('46', '2016-03-04 16:27:49', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:49', null, '0', null, null, null, null, null, null, '2', '测B00045', null, null, 'normal', '13100000045', null, '46', null, null, null, null);
INSERT INTO `vehicle` VALUES ('47', '2016-03-04 16:27:49', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:49', null, '0', null, null, null, null, null, null, '2', '测B00046', null, null, 'normal', '13100000046', null, '47', null, null, null, null);
INSERT INTO `vehicle` VALUES ('48', '2016-03-04 16:27:49', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:49', null, '0', null, null, null, null, null, null, '2', '测B00047', null, null, 'normal', '13100000047', null, '48', null, null, null, null);
INSERT INTO `vehicle` VALUES ('49', '2016-03-04 16:27:49', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:49', null, '0', null, null, null, null, null, null, '2', '测B00048', null, null, 'normal', '13100000048', null, '49', null, null, null, null);
INSERT INTO `vehicle` VALUES ('50', '2016-03-04 16:27:49', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:49', null, '0', null, null, null, null, null, null, '2', '测B00049', null, null, 'normal', '13100000049', null, '50', null, null, null, null);
INSERT INTO `vehicle` VALUES ('51', '2016-03-04 16:27:49', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:49', null, '0', null, null, null, null, null, null, '2', '测B00050', null, null, 'normal', '13100000050', null, '51', null, null, null, null);
INSERT INTO `vehicle` VALUES ('52', '2016-03-04 16:27:49', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:49', null, '0', null, null, null, null, null, null, '2', '测B00051', null, null, 'normal', '13100000051', null, '52', null, null, null, null);
INSERT INTO `vehicle` VALUES ('53', '2016-03-04 16:27:49', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:49', null, '0', null, null, null, null, null, null, '2', '测B00052', null, null, 'normal', '13100000052', null, '53', null, null, null, null);
INSERT INTO `vehicle` VALUES ('54', '2016-03-04 16:27:49', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:49', null, '0', null, null, null, null, null, null, '2', '测B00053', null, null, 'normal', '13100000053', null, '54', null, null, null, null);
INSERT INTO `vehicle` VALUES ('55', '2016-03-04 16:27:50', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:50', null, '0', null, null, null, null, null, null, '2', '测B00054', null, null, 'normal', '13100000054', null, '55', null, null, null, null);
INSERT INTO `vehicle` VALUES ('56', '2016-03-04 16:27:50', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:50', null, '0', null, null, null, null, null, null, '2', '测B00055', null, null, 'normal', '13100000055', null, '56', null, null, null, null);
INSERT INTO `vehicle` VALUES ('57', '2016-03-04 16:27:50', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:50', null, '0', null, null, null, null, null, null, '2', '测B00056', null, null, 'normal', '13100000056', null, '57', null, null, null, null);
INSERT INTO `vehicle` VALUES ('58', '2016-03-04 16:27:50', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:50', null, '0', null, null, null, null, null, null, '2', '测B00057', null, null, 'normal', '13100000057', null, '58', null, null, null, null);
INSERT INTO `vehicle` VALUES ('59', '2016-03-04 16:27:50', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:50', null, '0', null, null, null, null, null, null, '2', '测B00058', null, null, 'normal', '13100000058', null, '59', null, null, null, null);
INSERT INTO `vehicle` VALUES ('60', '2016-03-04 16:27:50', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:50', null, '0', null, null, null, null, null, null, '2', '测B00059', null, null, 'normal', '13100000059', null, '60', null, null, null, null);
INSERT INTO `vehicle` VALUES ('61', '2016-03-04 16:27:50', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:50', null, '0', null, null, null, null, null, null, '2', '测B00060', null, null, 'normal', '13100000060', null, '61', null, null, null, null);
INSERT INTO `vehicle` VALUES ('62', '2016-03-04 16:27:50', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:50', null, '0', null, null, null, null, null, null, '2', '测B00061', null, null, 'normal', '13100000061', null, '62', null, null, null, null);
INSERT INTO `vehicle` VALUES ('63', '2016-03-04 16:27:50', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:50', null, '0', null, null, null, null, null, null, '2', '测B00062', null, null, 'normal', '13100000062', null, '63', null, null, null, null);
INSERT INTO `vehicle` VALUES ('64', '2016-03-04 16:27:50', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:50', null, '0', null, null, null, null, null, null, '2', '测B00063', null, null, 'normal', '13100000063', null, '64', null, null, null, null);
INSERT INTO `vehicle` VALUES ('65', '2016-03-04 16:27:50', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:50', null, '0', null, null, null, null, null, null, '2', '测B00064', null, null, 'normal', '13100000064', null, '65', null, null, null, null);
INSERT INTO `vehicle` VALUES ('66', '2016-03-04 16:27:50', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:50', null, '0', null, null, null, null, null, null, '2', '测B00065', null, null, 'normal', '13100000065', null, '66', null, null, null, null);
INSERT INTO `vehicle` VALUES ('67', '2016-03-04 16:27:50', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:50', null, '0', null, null, null, null, null, null, '2', '测B00066', null, null, 'normal', '13100000066', null, '67', null, null, null, null);
INSERT INTO `vehicle` VALUES ('68', '2016-03-04 16:27:50', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:50', null, '0', null, null, null, null, null, null, '2', '测B00067', null, null, 'normal', '13100000067', null, '68', null, null, null, null);
INSERT INTO `vehicle` VALUES ('69', '2016-03-04 16:27:50', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:50', null, '0', null, null, null, null, null, null, '2', '测B00068', null, null, 'normal', '13100000068', null, '69', null, null, null, null);
INSERT INTO `vehicle` VALUES ('70', '2016-03-04 16:27:51', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:51', null, '0', null, null, null, null, null, null, '2', '测B00069', null, null, 'normal', '13100000069', null, '70', null, null, null, null);
INSERT INTO `vehicle` VALUES ('71', '2016-03-04 16:27:51', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:51', null, '0', null, null, null, null, null, null, '2', '测B00070', null, null, 'normal', '13100000070', null, '71', null, null, null, null);
INSERT INTO `vehicle` VALUES ('72', '2016-03-04 16:27:51', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:51', null, '0', null, null, null, null, null, null, '2', '测B00071', null, null, 'normal', '13100000071', null, '72', null, null, null, null);
INSERT INTO `vehicle` VALUES ('73', '2016-03-04 16:27:51', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:51', null, '0', null, null, null, null, null, null, '2', '测B00072', null, null, 'normal', '13100000072', null, '73', null, null, null, null);
INSERT INTO `vehicle` VALUES ('74', '2016-03-04 16:27:51', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:51', null, '0', null, null, null, null, null, null, '2', '测B00073', null, null, 'normal', '13100000073', null, '74', null, null, null, null);
INSERT INTO `vehicle` VALUES ('75', '2016-03-04 16:27:51', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:51', null, '0', null, null, null, null, null, null, '2', '测B00074', null, null, 'normal', '13100000074', null, '75', null, null, null, null);
INSERT INTO `vehicle` VALUES ('76', '2016-03-04 16:27:51', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:51', null, '0', null, null, null, null, null, null, '2', '测B00075', null, null, 'normal', '13100000075', null, '76', null, null, null, null);
INSERT INTO `vehicle` VALUES ('77', '2016-03-04 16:27:51', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:51', null, '0', null, null, null, null, null, null, '2', '测B00076', null, null, 'normal', '13100000076', null, '77', null, null, null, null);
INSERT INTO `vehicle` VALUES ('78', '2016-03-04 16:27:51', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:51', null, '0', null, null, null, null, null, null, '2', '测B00077', null, null, 'normal', '13100000077', null, '78', null, null, null, null);
INSERT INTO `vehicle` VALUES ('79', '2016-03-04 16:27:51', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:51', null, '0', null, null, null, null, null, null, '2', '测B00078', null, null, 'normal', '13100000078', null, '79', null, null, null, null);
INSERT INTO `vehicle` VALUES ('80', '2016-03-04 16:27:51', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:51', null, '0', null, null, null, null, null, null, '2', '测B00079', null, null, 'normal', '13100000079', null, '80', null, null, null, null);
INSERT INTO `vehicle` VALUES ('81', '2016-03-04 16:27:51', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:51', null, '0', null, null, null, null, null, null, '2', '测B00080', null, null, 'normal', '13100000080', null, '81', null, null, null, null);
INSERT INTO `vehicle` VALUES ('82', '2016-03-04 16:27:51', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:51', null, '0', null, null, null, null, null, null, '2', '测B00081', null, null, 'normal', '13100000081', null, '82', null, null, null, null);
INSERT INTO `vehicle` VALUES ('83', '2016-03-04 16:27:51', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:51', null, '0', null, null, null, null, null, null, '2', '测B00082', null, null, 'normal', '13100000082', null, '83', null, null, null, null);
INSERT INTO `vehicle` VALUES ('84', '2016-03-04 16:27:51', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:51', null, '0', null, null, null, null, null, null, '2', '测B00083', null, null, 'normal', '13100000083', null, '84', null, null, null, null);
INSERT INTO `vehicle` VALUES ('85', '2016-03-04 16:27:52', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:52', null, '0', null, null, null, null, null, null, '2', '测B00084', null, null, 'normal', '13100000084', null, '85', null, null, null, null);
INSERT INTO `vehicle` VALUES ('86', '2016-03-04 16:27:52', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:52', null, '0', null, null, null, null, null, null, '2', '测B00085', null, null, 'normal', '13100000085', null, '86', null, null, null, null);
INSERT INTO `vehicle` VALUES ('87', '2016-03-04 16:27:52', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:52', null, '0', null, null, null, null, null, null, '2', '测B00086', null, null, 'normal', '13100000086', null, '87', null, null, null, null);
INSERT INTO `vehicle` VALUES ('88', '2016-03-04 16:27:52', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:52', null, '0', null, null, null, null, null, null, '2', '测B00087', null, null, 'normal', '13100000087', null, '88', null, null, null, null);
INSERT INTO `vehicle` VALUES ('89', '2016-03-04 16:27:52', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:52', null, '0', null, null, null, null, null, null, '2', '测B00088', null, null, 'normal', '13100000088', null, '89', null, null, null, null);
INSERT INTO `vehicle` VALUES ('90', '2016-03-04 16:27:52', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:52', null, '0', null, null, null, null, null, null, '2', '测B00089', null, null, 'normal', '13100000089', null, '90', null, null, null, null);
INSERT INTO `vehicle` VALUES ('91', '2016-03-04 16:27:52', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:52', null, '0', null, null, null, null, null, null, '2', '测B00090', null, null, 'normal', '13100000090', null, '91', null, null, null, null);
INSERT INTO `vehicle` VALUES ('92', '2016-03-04 16:27:52', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:52', null, '0', null, null, null, null, null, null, '2', '测B00091', null, null, 'normal', '13100000091', null, '92', null, null, null, null);
INSERT INTO `vehicle` VALUES ('93', '2016-03-04 16:27:52', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:52', null, '0', null, null, null, null, null, null, '2', '测B00092', null, null, 'normal', '13100000092', null, '93', null, null, null, null);
INSERT INTO `vehicle` VALUES ('94', '2016-03-04 16:27:52', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:52', null, '0', null, null, null, null, null, null, '2', '测B00093', null, null, 'normal', '13100000093', null, '94', null, null, null, null);
INSERT INTO `vehicle` VALUES ('95', '2016-03-04 16:27:52', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:52', null, '0', null, null, null, null, null, null, '2', '测B00094', null, null, 'normal', '13100000094', null, '95', null, null, null, null);
INSERT INTO `vehicle` VALUES ('96', '2016-03-04 16:27:52', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:52', null, '0', null, null, null, null, null, null, '2', '测B00095', null, null, 'normal', '13100000095', null, '96', null, null, null, null);
INSERT INTO `vehicle` VALUES ('97', '2016-03-04 16:27:52', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:52', null, '0', null, null, null, null, null, null, '2', '测B00096', null, null, 'normal', '13100000096', null, '97', null, null, null, null);
INSERT INTO `vehicle` VALUES ('98', '2016-03-04 16:27:52', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:52', null, '0', null, null, null, null, null, null, '2', '测B00097', null, null, 'normal', '13100000097', null, '98', null, null, null, null);
INSERT INTO `vehicle` VALUES ('99', '2016-03-04 16:27:52', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:52', null, '0', null, null, null, null, null, null, '2', '测B00098', null, null, 'normal', '13100000098', null, '99', null, null, null, null);
INSERT INTO `vehicle` VALUES ('100', '2016-03-04 16:27:52', '', null, null, '0', null, '117440514', null, null, null, null, null, '2016-03-04 16:27:52', null, '0', null, null, null, null, null, null, '2', '测B00099', null, null, 'normal', '13100000099', null, '100', null, null, null, null);

-- ----------------------------
-- Table structure for `vehicleonlinerate`
-- ----------------------------
DROP TABLE IF EXISTS `vehicleonlinerate`;
CREATE TABLE `vehicleonlinerate` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `intervalType` int(11) NOT NULL,
  `offlineTime` double NOT NULL,
  `onlineRate` double NOT NULL,
  `onlineTime` double NOT NULL,
  `plateNo` varchar(255) DEFAULT NULL,
  `staticDate` datetime DEFAULT NULL,
  `totalTime` double NOT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Id` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vehicleonlinerate
-- ----------------------------

-- ----------------------------
-- Table structure for `warnmsgurgtodoreq`
-- ----------------------------
DROP TABLE IF EXISTS `warnmsgurgtodoreq`;
CREATE TABLE `warnmsgurgtodoreq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `tenantId` int(11) NOT NULL,
  `ackFlag` int(11) NOT NULL,
  `ackTime` datetime DEFAULT NULL,
  `origin` int(11) NOT NULL,
  `plateColor` int(11) NOT NULL,
  `plateNo` varchar(255) DEFAULT NULL,
  `result` int(11) NOT NULL,
  `supervicsionId` int(11) NOT NULL,
  `supervisionEndtime` datetime DEFAULT NULL,
  `supervisionLevel` int(11) NOT NULL,
  `supervisor` varchar(255) DEFAULT NULL,
  `supervisorEmail` varchar(255) DEFAULT NULL,
  `supervisorTel` varchar(255) DEFAULT NULL,
  `vehicleId` int(11) DEFAULT NULL,
  `warnSrc` int(11) NOT NULL,
  `warnTime` datetime DEFAULT NULL,
  `warnType` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of warnmsgurgtodoreq
-- ----------------------------
