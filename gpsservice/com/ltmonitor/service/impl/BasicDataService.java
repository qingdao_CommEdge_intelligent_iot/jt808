package com.ltmonitor.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.ltmonitor.dao.impl.DaoHibernateImpl;
import com.ltmonitor.entity.BasicData;
import com.ltmonitor.service.IBasicDataService;


public class BasicDataService extends DaoHibernateImpl implements IBasicDataService {

	
	public List getBaseDataByParentID(Serializable parentID) {
		String queryString = "from BasicData b where b.parent = ? and b.deleted=false order by b.name ASC";
		return query(queryString, parentID);

	}

	
	public BasicData getBasicDataByCode(String code, String parentCode) {
		BasicData bd = (BasicData) this.find(
				"from BasicData a where a.code = ? and a.parent = ?", new Object[]{code, parentCode});

		return bd;
	}

	
	public BasicData getBasicDataByName(String name, String parentCode) {
		List ls = this.getBasicDataByParentCode(parentCode);

		for (int m = 0; m < ls.size(); m++) {
			BasicData bd = (BasicData) ls.get(m);

			if (name.equals(bd.getName()))
				return bd;
		}

		return null;
	}

	
	public List<BasicData> getBasicDataByParentCode(String parentCode) {
		
		String queryString = "from BasicData b where b.parent = ?  and b.deleted=false order by b.name ASC";

		List rs =  query(queryString, parentCode);
		List<BasicData> result = new ArrayList<BasicData>();
		for(Object obj : rs)
		{
			result.add((BasicData)obj);
		}
		return result;
	}

	
	public BasicData getBasicDataByID(Serializable ID) {
		return (BasicData) super.load(BasicData.class, ID);
	}

	
	

}
