package com.ltmonitor.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;


@Entity
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Table(name = "Terminal", uniqueConstraints = { @javax.persistence.UniqueConstraint(columnNames = { "termNo" }) })
public class Terminal extends TenantEntity {
	private static final long serialVersionUID = -480253912021549418L;
	private int entityId;
	//出厂号
	private String devNo = "";
	//设备编号
	private String termNo = "";
	private Short verSoftware = 0;
	private Short verHardware = 0;
	private Short verProtocol = 0;
	//生产厂家
	private String makeFactory = "";
	private Date makeTime = new Date();
	//生产批次
	private String makeNo = "";
	//状态
	private String state;
	private Date installTime = new Date();
	//安装工
	private String waitor = "";
	private String installAddress = "";
	//终端型号
	private String termType;
	//是否已经绑定
	private Boolean bind = false;

	private Date updateTime = new Date();
	//流水号
	private String seqNo;

	public Terminal() {

	}

	private String simNo;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "termId", unique = true, nullable = false)
	public int getEntityId() {
		return this.entityId;
	}

	public void setEntityId(int id) {
		this.entityId = id;
	}

	@Column(name = "devNo", nullable = true, length = 20)
	public String getDevNo() {
		return this.devNo;
	}

	public void setDevNo(String devNo) {
		this.devNo = devNo;
	}

	@Column(name = "termNo", unique = true, nullable = true, length = 20)
	public String getTermNo() {
		return this.termNo;
	}

	public void setTermNo(String termNo) {
		this.termNo = termNo;
	}

	@Column(name = "verSoftware", nullable = true)
	public Short getVerSoftware() {
		return this.verSoftware;
	}

	public void setVerSoftware(Short verSoftware) {
		this.verSoftware = verSoftware;
	}

	@Column(name = "verHardware", nullable = true)
	public Short getVerHardware() {
		return this.verHardware;
	}

	public void setVerHardware(Short verHardware) {
		this.verHardware = verHardware;
	}

	@Column(name = "verProtocol", nullable = true)
	public Short getVerProtocol() {
		return this.verProtocol;
	}

	public void setVerProtocol(Short verProtocol) {
		this.verProtocol = verProtocol;
	}

	@Column(name = "makeFactory", nullable = true, length = 30)
	public String getMakeFactory() {
		return this.makeFactory;
	}

	public void setMakeFactory(String makeFactory) {
		this.makeFactory = makeFactory;
	}

	@Column(name = "makeTime")
	public Date getMakeTime() {
		return this.makeTime;
	}

	public void setMakeTime(Date makeTime) {
		this.makeTime = makeTime;
	}

	@Column(name = "makeNo", nullable = true, length = 10)
	public String getMakeNo() {
		return this.makeNo;
	}

	public void setMakeNo(String makeNo) {
		this.makeNo = makeNo;
	}

	@Column(name = "state", nullable = true)
	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}


	@Column(name = "installTime")
	public Date getInstallTime() {
		return this.installTime;
	}

	public void setInstallTime(Date installTime) {
		this.installTime = installTime;
	}

	@Column(name = "waitor", length = 30)
	public String getWaitor() {
		return this.waitor;
	}

	public void setWaitor(String waitor) {
		this.waitor = waitor;
	}

	@Column(name = "installAddress", length = 80)
	public String getInstallAddress() {
		return this.installAddress;
	}

	public void setInstallAddress(String installAddress) {
		this.installAddress = installAddress;
	}

	@Column(name = "termType")
	public String getTermType() {
		return this.termType;
	}

	public void setTermType(String termType) {
		this.termType = termType;
	}

	@Column(name = "updateTime")
	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getSimNo() {
		return simNo;
	}

	public void setSimNo(String simNo) {
		this.simNo = simNo;
	}

	public String getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}

	public Boolean isBind() {
		return bind;
	}

	public void setBind(Boolean bind) {
		this.bind = bind;
	}
}