﻿package com.ltmonitor.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name="mediaItem")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) 
public class MediaItem extends TenantEntity
{
	public static final String UPLOAD = "upload"; //媒体数据上传
	public static final String SEARCH = "search"; //媒体检索项
	public static final String INFO = "search"; //媒体事件信息

	public MediaItem()
	{
		setCommandType(UPLOAD);
		setCreateDate(new java.util.Date());
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "mediaItemId", unique = true, nullable = false)
	private int entityId;
	public  int getEntityId() {
		return entityId;
	}
	public  void setEntityId(int value) {
		entityId = value;
	}
	
	private String plateNo;
	public final String getPlateNo()
	{
		return plateNo;
	}
	public final void setPlateNo(String value)
	{
		plateNo = value;
	}

	private String simNo;
	public final String getSimNo()
	{
		return simNo;
	}
	public final void setSimNo(String value)
	{
		simNo = value;
	}

	//发生时间
	private java.util.Date sendTime = new java.util.Date(0);
	public final java.util.Date getSendTime()
	{
		return sendTime;
	}
	public final void setSendTime(java.util.Date value)
	{
		sendTime = value;
	}

	/** 多媒体数据ID
	 
	*/
	private int mediaDataId;
	/** 
	 多媒体类型
	 
	*/
	private byte mediaType;
	/** 
	 多媒体格式编码
	 
	*/
	private byte codeFormat;
	
	private double latitude;
	
	private double longitude;
	
	private double speed;
	
	//private String alarmState;	
	
	/** 
	 事件项编码
	 
	*/
	private byte eventCode;
	public final byte getEventCode()
	{
		return eventCode;
	}
	public final void setEventCode(byte value)
	{
		eventCode = value;
	}
	/** 
	 通道ID
	 
	*/
	private byte channelId;
	public final byte getChannelId()
	{
		return channelId;
	}
	public final void setChannelId(byte value)
	{
		channelId = value;
	}

	//数据存储的文件名
	private String fileName;
	public final String getFileName()
	{
		return fileName;
	}
	public final void setFileName(String value)
	{
		fileName = value;
	}

	//根据那个命令返回的ID，通过此命令可以查询出所返回的数据
	private int commandId;
	public final int getCommandId()
	{
		return commandId;
	}
	public final void setCommandId(int value)
	{
		commandId = value;
	}
	//upload 上传命令，search 检索命令, 检索不返回文件数据
	private String commandType;
	public final String getCommandType()
	{
		return commandType;
	}
	public final void setCommandType(String value)
	{
		commandType = value;
	}
	public int getMediaDataId() {
		return mediaDataId;
	}
	public void setMediaDataId(int mediaDataId) {
		this.mediaDataId = mediaDataId;
	}
	public byte getMediaType() {
		return mediaType;
	}
	public void setMediaType(byte mediaType) {
		this.mediaType = mediaType;
	}
	public byte getCodeFormat() {
		return codeFormat;
	}
	public void setCodeFormat(byte codeFormat) {
		this.codeFormat = codeFormat;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
}