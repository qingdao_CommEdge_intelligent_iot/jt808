﻿package com.ltmonitor.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import com.ltmonitor.util.DateUtil;


//车辆基本静态信息

@Entity
@Table(name="Vehicle")
@org.hibernate.annotations.Proxy(lazy = false)
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS) 
public class VehicleData extends TenantEntity 
{
	public static String ONLINE = "online";
	public static String OFFLINE = "offline";
	
	public VehicleData()
	{
		//setPlateNo("未注册车");

		setInstallDate(DateUtil.now());
		setCreateDate(DateUtil.now());
		setRunStatus("normal");
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "vehicleId", unique = true, nullable = false)
	private int entityId;
	public  int getEntityId() {
		return entityId;
	}
	public  void setEntityId(int value) {
		entityId = value;
	}
	
	//车牌号
	@Column(name = "plateNo")
	private String plateNo;
	public final String getPlateNo()
	{
		return plateNo;
	}
	public final void setPlateNo(String value)
	{
		plateNo = value;
	}
	//车辆类型,在基础数据表中定义各个车型
	@Column(name = "vehicleType")
	private String vehicleType;
	//厂商型号
	private String vendor;
	//使用类型
	private String useType;
	//车籍地
	private String region;
	//经营许可
	private String operPermit;
	//购买日期
	private Date buyTime;
	//所属业户Id
	private int memberId;
	
	//private boolean registered;

	//所属行业，在基础数据里定义
	@Column(name = "industry")
	private String industry;
	public final String getIndustry()
	{
		return industry;
	}
	public final void setIndustry(String value)
	{
		industry = value;
	}

	
	//配送员
	@Column(name = "monitor")
	private String monitor;
	public final String getMonitor()
	{
		return monitor;
	}
	public final void setMonitor(String value)
	{
		monitor = value;
	}
	
	//行驶状态，正常normal，报废scrape，维修maintaining，等等
	@Column(name = "runStatus")
	private String runStatus;
	public final String getRunStatus()
	{
		return runStatus;
	}
	public final void setRunStatus(String value)
	{
		runStatus = value;
	}
	//司机
	private String driver;
	public final String getDriver()
	{
		return driver;
	}
	public final void setDriver(String value)
	{
		driver = value;
	}
	//GPS 在线状态
	private String status;
	public final String getStatus()
	{
		return status;
	}
	public final void setStatus(String value)
	{
		status = value;
	}
	//车牌颜色
	private int plateColor;
	//终端Id
	private int termId;
	
	//所属部门ID
	private int depId;
	public final int getDepId()
	{
		return depId;
	}
	public final void setDepId(int value)
	{
		depId = value;
	}

	//GPS车机类型,使用不同终端设备的类型
	private String gpsTerminalType;
	public final String getGpsTerminalType()
	{
		return gpsTerminalType;
	}
	public final void setGpsTerminalType(String value)
	{
		gpsTerminalType = value;
	}

	//视频设备的唯一ID
	private String videoDeviceId;
	public final String getVideoDeviceId()
	{
		return videoDeviceId;
	}
	public final void setVideoDeviceId(String value)
	{
		videoDeviceId = value;
	}
	//发动机号
	private String motorID;
	public final String getMotorID()
	{
		return motorID;
	}
	public final void setMotorID(String value)
	{
		motorID = value;
	}

	//设备入网安装日期
	private java.util.Date installDate;
	public final java.util.Date getInstallDate()
	{
		return installDate;
	}
	public final void setInstallDate(java.util.Date value)
	{
		installDate = value;
	}

	//GPS手机卡号
	private String simNo;
	//司机电话
	private String driverMobile;
	public final String getDriverMobile()
	{
		return driverMobile;
	}
	public final void setDriverMobile(String value)
	{
		driverMobile = value;
	}
	//押运员电话
	private String monitorMobile;
	public final String getMonitorMobile()
	{
		return monitorMobile;
	}
	public final void setMonitorMobile(String value)
	{
		monitorMobile = value;
	}
	//所属部门名称
	private String depName;
	public final String getDepName()
	{
		return depName;
	}
	public final void setDepName(String value)
	{
		depName = value;
	}
	
	//最新在线时间
	private java.util.Date onlineTime;
	public final java.util.Date getOnlineTime()
	{
		return onlineTime;
	}
	public final void setOnlineTime(java.util.Date value)
	{
		onlineTime = value;
	}
	//上次离线时间
	private java.util.Date offlineTime;
	public final java.util.Date getOfflineTime()
	{
		return offlineTime;
	}
	public final void setOfflineTime(java.util.Date value)
	{
		offlineTime = value;
	}

	//设备巡检时间
	private java.util.Date lastCheckTime;
	public final java.util.Date getLastCheckTime()
	{
		return lastCheckTime;
	}
	public final void setLastCheckTime(java.util.Date value)
	{
		lastCheckTime = value;
	}
	//绑定的路线ID，多条路线以,号分割
	private String routes;
	public final String getRoutes()
	{
		return routes;
	}
	public final void setRoutes(String value)
	{
		routes = value;
	}
	public String getSimNo() {
		return simNo;
	}
	public void setSimNo(String simNo) {
		this.simNo = simNo;
	}
	public int getPlateColor() {
		return plateColor;
	}
	public void setPlateColor(int plateColor) {
		this.plateColor = plateColor;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public int getTermId() {
		return termId;
	}
	public void setTermId(int termId) {
		this.termId = termId;
	}
	public String getOperPermit() {
		return operPermit;
	}
	public void setOperPermit(String operPermit) {
		this.operPermit = operPermit;
	}
	public String getUseType() {
		return useType;
	}
	public void setUseType(String useType) {
		this.useType = useType;
	}
	public Date getBuyTime() {
		return buyTime;
	}
	public void setBuyTime(Date buyTime) {
		this.buyTime = buyTime;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public int getMemberId() {
		return memberId;
	}
	public void setMemberId(int memberId) {
		this.memberId = memberId;
	}
}