﻿package com.ltmonitor.entity;

public class EnclosureNode
{
	private double lat;
	public final double getLat()
	{
		return lat;
	}
	public final void setLat(double value)
	{
		lat = value;
	}

	private double lng;
	public final double getLng()
	{
		return lng;
	}
	public final void setLng(double value)
	{
		lng = value;
	}

}